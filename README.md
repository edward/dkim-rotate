% dkim-rotate - rotation and revocation of DKIM keys

`dkim-rotate` is a tool for managing [DKIM] (email antispam) keys
in a manner that avoids unnecessarily making emails nonrepudiable.

Broadly, dkim-rotate intends to weaken the non-deniable authenticity of
leaked and archived emails,
while still retaining DKIM's antispam function.
For more discussion of the problem,
and the chosen solution,
see [dkim-rotate(7)],
and
Matthew Green's article
[_Ok Google: please publish your DKIM secret keys_][Green].

Features and benefits
---------------------

 * Leaked emails become unattestable (plausibily deniable)
   within a few days ---
   soon after the configured maximum email propagation time.

 * Mail domain DNS configuration can be static, 
   and separated from operational DKIM key rotation.
   Domain owner delegates DKIM configuration
   to mailserver administrator, so that
   dkim-rotate does not need to edit your mail domain's zone.

 * When a single mail server handles multiple mail domains,
   only a single dkim-rotate instance is needed.

 * Supports situations where
   multiple mail servers may originate mails for a single mail domain.

 * DNS zonefile remains small;
   old keys are published via a webserver, rather than DNS.

 * Supports runtime (post-deployment)
   changes to tuning parameters and configuration settings.
   Careful handling of errors and out-of-course situations.

 * Convenient outputs:
   a standard DNS zonefile;
   easily parseable settings for the MTA;
   and,
   a directory of old keys directly publishable by a webserver.

Requirements
------------

 * dkim-rotate maintains a dedicated DNS zone for the DKIM keys.
   You will need a nameserver, and suitable secondaries.

 * Mail server software must be able to read
   a dynamic configuration file
   for DKIM selector and key information.
   (Example configuration for Exim is provided.)

 * Web server for publication of expired private keys.

 * Fairly minimal dependencies:
   perl and a few modules,
   the `openssl(1)` command line utility,
   and basic utilities like `bash`, `curl` and `md5sum`.

Canonical location
------------------

dkim-rotate is hosted on Debian's gitlab instance, Salsa:

<https://salsa.debian.org/iwj/dkim-rotate>

The formatted documentation is mirrored here:

<https://www.chiark.greenend.org.uk/~ianmdlvl/dkim-rotate/>

Releases are made by signed git tags,
uploaded to Debian,
and announced on the
[sgo-software-announce](https://www.chiark.greenend.org.uk/pipermail/sgo-software-announce/)
mailing list.

References and documentation
----------------------------

  * [dkim-rotate(7)].
    Principles of Operation.
    Output file formats.
    DNS and MTA integration (including examples).

  * [dkim-rotate(1)]. Command line reference.

  * [dkim-rotate(5)]. Configuration.

  * [RFC6376][DKIM] DomainKeys Identified Mail (DKIM) Signatures.

  * Matthew Green, [_Ok Google: please publish your DKIM secret keys_][Green].
    Explanation of the problem,
    calling on big mail server operators to do as dkim-rotate does.

  * [INTERNALS](INTERNALS.html).
    Internal docs - algorithms and file formats.

[dkim-rotate(1)]: dkim-rotate.1.html
[dkim-rotate(5)]: dkim-rotate.5.html
[dkim-rotate(7)]: dkim-rotate.7.html
[DKIM]: https://datatracker.ietf.org/doc/html/rfc6376
[Green]: https://blog.cryptographyengineering.com/2020/11/16/ok-google-please-publish-your-dkim-secret-keys/

COPYRIGHT AND AUTHORSHIP
========================

Copyright 2022 Ian Jackson and contributors to dkim-rotate.

There is **NO WARRANTY**.

SPDX-License-Identifier: GPL-3.0-or-later`
