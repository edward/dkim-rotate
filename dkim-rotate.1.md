% dkim-rotate(1)

NAME
===

`dkim-rotate` - rotate and revoke and invalidate DKIM keys

SYNOPSIS
========

| `dkim-rotate` [_options_] `--new` [_instance_ ...]
| `dkim-rotate` [_options_] `--major` [_instance_..]
| `dkim-rotate` [_options_] `--minor` [_instance_  ...]
| `dkim-rotate` [_options_] `--status` [_instance_ ...]
| `dkim-rotate` [_options_] `--reinstall` [_instance_ ...]

DESCRIPTION
===========

`dkim-rotate` is a tool for managing DKIM (email antispam) keys
in a manner that avoids unnecessarily making emails nonrepudiable.

For each instance,
`dkim-rotate` maintains several keys concurrently,
using "selectors" in a circular rotation.

See **dkim-rotate(7)**
for the Principles of Operation,
and details of how to configure your MTA, DNS, and WWW server.

If no _instance_ is provided,
`dkim-rotate` will operate on all instances
matching `[a-z][-_0-9a-z]*`
for which the configuration file
`/etc/dkim-rotate/`_instance_`.zone`
exists.

See **dkim-rotate(5)** for details about the instance configuration file.

If an _instance_ is provided
and contains a slash, it will be treated as a pathname;
otherwise it will be taken as a reference to
the configuration file in `/etc`.

`dkim-rotate` should normally be run out of cron.
It will produce progress information on stdout.
It will produce stderr output if and only if something is wrong.

MODE OPTIONS
============

`--major`

:   Make progress.
    Create new keys, advance to using different keys, 
    and reveal old keys, as necessary.

`--minor`

:   Make progress, but do not advance to using a new key.
    If you wish your keys to be rotated
    at particular times of the day or week,
    you should run with `--major` at those times,
    and `--minor` otherwise.

    For example, the suggested/default configuration runs
    with `--major` at 0400 local time.
    The effect is that emails sent on a particular day
    all cease to be repudiable at the same time.

`--new`

:   Make progress, and, additionally,
    allow the creation of a new instance.
    Without `--new`,
    it is an error if there is a config file,
    but no recorded state.

`--reinstall`

:   Do not make any progress,
    but force recreation, reinstallation and reload of
    MTA and DNS output files.

`--status`

:   Produce a status report of all the relevant keys.
    Do not make any changes.


OTHER OPTIONS
=============

`--etc-dir`=_etc-dir_

:   Look for instance configuration files in _etc-dir_
    rather than `/etc/dkim-rotate`.


`--var-dir`=_var-dir_

:   Look for instance state directories in _var-dir_
    rather than `/var/lib/dkim-rotate`.

AUTHOR
======

| Copyright 2022 Ian Jackson and contributors to dkim-rotate.
| There is NO WARRANTY.
| `SPDX-License-Identifier: GPL-3.0-or-later`

SEE ALSO
========

dkim-rotate(5)

:   Configuration file

dkim-rotate(7)

:   Principles of Operation

RFC6376

:   DKIM Signatures
